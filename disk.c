#include "disk.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

char** disk = NULL;

int g_block_size = 512;
int g_block_num  = 256;

int g_fdrw_ctr = 0;

/*
 * reallocate disk when block size and num change?
 */

void set_block_size (int sz)
{
  g_block_size = sz;
}

int get_block_size ()
{
  return g_block_size;
}

void set_block_num (int block_num)
{
  g_block_num = 0;
}

int get_block_num ()
{
  return g_block_num;
}

static bool within (int x, int lwrlim, int uprlim)
{
  return ((x >= lwrlim) &&
	  (x <= uprlim));
}

/* find better names asap */
static bool exists (int b)
{
  return (disk &&
	  disk[b]);
}

static void makblock (int b) 
{
  if (!disk)
    disk = calloc(get_block_num(), sizeof(char*));
  if (!disk[b])
    disk[b] = calloc(get_block_size(), sizeof(char));
}

/* ok so this works for now and is less stuff to read 
 * so i'll use this i guess
 */
static void fd_read (int block, void* buf, int sz)
{
  if (within(block, 0, get_block_num() - 1))
    if (exists(block)) {
      memcpy(buf, disk[block], sz);
      g_fdrw_ctr++;
    }
}

static void fd_write (int block, void* buf, int sz)
{
  if (within(block, 0, get_block_num() - 1)) {
    makblock(block);
    
    memcpy(disk[block], buf, sz);
    g_fdrw_ctr++;
  }  
}

void fd_read_raw (int block, char* buf)
{
  fd_read(block, buf, get_block_size());
}

void fd_write_raw (int block, char* buf)
{
  fd_write(block, buf, get_block_size());
}

void fd_read_super_block (int block, struct super_block* buf)
{
  fd_read(block, buf, sizeof(struct super_block));
}

void fd_write_super_block(int block, struct super_block * buf)
{
  fd_write(block, buf, sizeof(struct super_block));
}

void fd_read_i_node_block(int block, struct i_node_block * buf)
{
  
}

void fd_write_i_node_block(int block, struct i_node_block * buf)
{

}

void fd_read_indirect_block(int block, struct indirect_block * buf)
{

}

void fd_write_indirect_block(int block, struct indirect_block * buf)
{

}

int fd_stop()
{
  int i;
  if (disk)
    for (i = 0; i < g_block_num; i++)
      if (disk[i])
	free(disk[i]);
  free(disk);
  
  return g_fdrw_ctr;
}

#ifdef DISK_MAIN

#include <stdio.h>

main ()
{
  struct super_block  a;
  struct super_block* b;

  a.sz  = get_block_num();
  a.isz = get_block_num() / 2;
  a.free_lst = 1;

  b = calloc(1, sizeof(struct super_block));

  fd_write_super_block(0, &a);
  fd_read_super_block (0,  b);

  printf("%d %d %d\n", b->sz, b->isz, b->free_lst);

  printf("%d\n", fd_stop());

  free(b);
  return 0;
}

#endif /*DISK_MAIN*/
