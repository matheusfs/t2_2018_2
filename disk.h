#ifndef _FAKEDISK_H_
#define _FAKEDISK_H_

#define I_NODE_SIZE 64

struct i_node{
	int size;
	int flags;
	int owner;
	int file_size; 
	int *pointer; // deve ser alocado na inicializacao
};



struct i_node_block {
  /* 
   * ok so i was actually wrong.. i think 
   * the individual i_nodes are stored and not the pointers to them
   */
  struct i_node* node;
};

struct indirect_block {
  /* apparently is a bunch of pointers to other blocks? */
};

struct super_block {
  int sz;       /* number of blocks */
  int isz;      /* number of index blocks */
  int free_lst; /* first block in the free list */
};


/* Funcoes para mudar e obter o tamanho e numero de blocos simulados */
void set_block_size(int block_size); //default 512
int get_block_size();

void set_block_num(int block_num); //default 256
int get_block_num();



/* Como definido no livro */
void fd_read_raw(int block_num, char * buffer);

void fd_write_raw(int block_num, char * buffer);

void fd_read_super_block(int block_num, struct super_block * buffer);

void fd_write_super_block(int block_num, struct super_block * buffer);

void fd_read_i_node_block(int block_num, struct i_node_block * buffer);

void fd_write_i_node_block(int block_num, struct i_node_block * buffer);

void fd_read_indirect_block(int block_num, struct indirect_block * buffer);

void fd_write_indirect_block(int block_num, struct indirect_block * buffer);

int fd_stop();


#endif /* _FAKEDISK_H_ */
